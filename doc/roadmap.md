# Roadmap neuer Bilderpool

Ziel ist zuerst die Erhaltung des Status quo (nur lesbarer Bilderpool), dann Wiederherstellung einer Benutzung (Hinzufügung neuer Elemente) und erst zuletzt das Hinzufügen von neuen Features.

1. Bilderpool mit importierten Bildern, Kategorien und Kommentaren des alten Bilderpools und Suchfunktion.
Nur anzeige der Elemente. Diese Version wird öffentlich ausgerollt, da diese großflächig getestet werden muss um Fehler im Import zu finden.
Ein erneuter Import nach Änderung von Elementen (Schritt 3) würde zu Datenverlust (z.B. Entfernung von Kommentaren) führen und wird deswegen ausgeschlossen.

Zusätzlich enthält diese Version das Basis-Layout sowie weitere statische Seiten, z.B. Chat und ftpedia aber keine dynamischen wie z.B. Wiki

Mehrwöchige Testphase.
Voraussetzung: Datenbestand des alten Bilderpools

2. Authentifizierung gegen Forum
Wieder öffentliche Version. Test ob bestehende Bilder und Kommentare dem richtigen Benutzer zugeordnet werden, z.B. Anhand der Seiten "meine Bilder" und "meine Kommentare"

Mehrwöchige Testphase.
Voraussetzung: Schnittstelle zur Authentifizierung in phpBB3

3. Hinzufügen von neuen Bildern und Kommentare
Ab hier kein erneuter Import möglich

4. Implementation neuer Features
z.B. Tags
