# Systemarchitektur für <https://ftcommunity.de>

Für alle Lösungen wird Open-Source-Software genutzt.

## Lösung A: Verwendung eines dynamischen Content-Management-Systems

Ein _dynamisches_ oder _klassisches Content-Management-System (CMS)_ ist ein Programm,
das auf dem Server läuft und dort
dynamisch Webseiten generiert,
die dann zum Beispiel von den Rechten des angemeldeten Benutzers abhängen.
Die Daten dafür sind in einer Datenbank abgelegt.
Beispiel: Drupal, in der Programmiersprache PHP geschrieben.

### Vorteile
* Ein CMS kann wahrscheinlich alle Anforderungen (Benutzerverwaltung, Rechteverwaltung, Bilderpool) umsetzen.
* Die Absicherung erfolgt durch Sicherheitsupdates, die von den Entwicklern des CMS bereitgestellt werden.
* Die Anpassung an eigene Vorstellungen erfolgt durch die Konfiguration diverser Parameter.

### Nachteile
* Die Installation und Einrichtung ist abhängig von der Mächtigkeit des CMS nicht trivial.
* Ein großes CMS ist ein komplexes Programm mit einer unübersichtlichen Architektur.
* Durch die dynamische Seitengenerierung, den Einsatz von PHP und einer Datenbank 
  ergeben sich viele Möglichkeiten für Sicherheitslücken.
* Für die Sicherheitsupdates ist man auf die Entwickler-Community des CMS angewiesen.

 
## Lösung B: Komplette eigene Programmierung mit einem Webframework (hier: Ruby on Rails)

### Vorteile
* Man ist nicht so festgelegt wie bei einem CMS
* Anforderungen können schnell implementiert werden
* Klare Trennung von Logik und Darstellung durch Model-View-Controller Architektur
* Konvention vor Konfiguration bedeutet schnelle Einarbeitung durch neue Entwickler, hohe Wartbarkeit und übersichtliche Architektur auch bei großen Projekten. Es ist wenig Dokumentation erforderlich.
* Sicherheitslücken werden durch Konventionen des Frameworks und langjährige Reifung der verwendeten Module vermieden.
* Versorgung von Sicherheitsupdates durch das Framework und verwendeten Module
* Integration von Unit- und Integrationstests Bestandteil der Architektur
* Einfache Implementierung einer REST API, z.B. für Publisher

### Nachteile
* Updates auf neue Major-Versionen sehr aufwendig, siehe z.B. [github](https://githubengineering.com/upgrading-github-from-rails-3-2-to-5-2/)
* Installation und Konfiguration nicht trivial, hier ist Unterstützung durch einen Entwickler erforderlich. Der sollte zwangsläufig vorhanden sein.
* Einzelne verwendete Module (gems) werden u.U. im Laufe der Zeit von den jeweiligen Entwicklern nicht mehr unterstützt. Hier sind dann umfangreiche Umbaumaßnahmen erforderlich
* Derzeit nicht auf Windows-Servern lauffähig
* Langsame Interpretersprache
* Kenntnisse in mehreren Programmiersprachen (Ruby, JavaScript) erforderlich
* Nur Backend, zusätzliche JavaScript-Frameworks wie z.B. jQuery und Vue.js im Frontend erforderlich

### Mögliche Module
* [Upload von Dateien: carrierwave](https://github.com/carrierwaveuploader/carrierwave)
* [Thumpnail Generation: minimagick](https://github.com/minimagick/minimagick)
* [Benutzerverwaltung: devise](https://github.com/plataformatec/devise)
* [CSS Framework Bootstrap](https://github.com/twbs/bootstrap-sass)

## Lösung C: Komplette eigene Programmierung mit einem modernem Webframework z.B AngularJs, React oder Vuejs

### Vorteile
* Modernes Framework
* Vollständig im Frontend, Backend stellt nur die Datenbank zur Verfügung

## Lösung D: Baukastensystem aus Standardkomponenten

Der Baukasten enthält die folgenden Teile:
* Eine Komponente für die Speicherung und Verwaltung der eigentlichen Inhalte.
  Das muss nicht unbedingt so wie z.B. beim klassischen CMS eine Datenbank sein,
  sondern kann auch ein Versionsverwaltungssystem 
  wie z.B. [Git](https://git-scm.com/), [Mercurial](https://www.mercurial-scm.org/) oder [Subversion](https://subversion.apache.org/) sein.
  Die (Text-)Inhalte selbst werden bevorzugt in einer einfach editierbaren 
  und einfach in andere Formate umwandelbaren Form gespeichert,
  wie z.B. [Markdown](https://commonmark.org/)
* Eine Komponente, die aus Markdown fertige, direkt auslieferbare HTML-Seiten macht.
  Beispiele: [Hugo](https://gohugo.io/), [Jekyll](https://jekyllrb.com/) und [viele andere](https://www.staticgen.com/)
* Eine Komponente, die die fertigen Seiten ausliefert.
  Hier geht eigentlich jeder Webserver. 
  Natürlich auch [Nginx](https://nginx.org/), [Apache](https://httpd.apache.org/) und die ganzen anderen bekannten Namen.
* Eine Komponente, die in die ausgelieferten statischen Seiten die gewünschte Dynamik bringt
  (wie z.B. die zufällig aus dem Bilderpool gezogenen Bilder auf der Startseite).
  Diese Komponente läuft im Browser des Benutzer 
  und wird in [JavaScript](https://de.wikipedia.org/wiki/JavaScript) 
  oder einer Variante davon wie [TypeScript](https://www.typescriptlang.org/) umgesetzt.
  Zusätzlich kann man sich auf fertige Bibliotheken und Frameworks wie bei Lösung C genannt stützen.
* Eine Komponente, mit der man die Inhalte der Website verändern kann.
  Hier gibt es eine immense Auswahl, 
  angefangen von Git und einem beliebigen Texteditor auf dem lokalen PC eines "Redakteurs",
  über Git-Weboberflächen wie [Gitea](https://gitea.io/en-us/) oder sogar [Gitlab](https://about.gitlab.com/install/)
  (ja, das kann man auf dem eigenen Server installieren),
  bis hin zu fertigen CMS-Oberflächen wie [Netlify](https://www.netlifycms.org/).
  Und natürlich ebenfalls die Option,
  selbst eine passende Weboberfläche maßzuschneidern.
* Und last not least eine Komponente,
  die sich darum kümmert den Zugang zu den anderen Komponenten zu regeln.
  Auch hier gibt mehre Möglichkeiten, von fertigen Lösungen wie z.B. [Keycloak](https://www.keycloak.org/)
  über Baukastensysteme wie das von [Ory](https://github.com/ory),
  die die Zugangskontrolle selbst nochmal in einzelne Komponenten aufteilen,
  bis hin zum Selbstbau (wobei Selbstbau gerade hier aus Sicherheitsgründen eher keine gute Idee ist).

Die meisten Komponenten dieses Baukastens sind entweder von vornherein darauf ausgerichtet,
mit dem Rest der Welt nur über eine vergleichsweise schmale Schnittstelle zu reden,
die zudem noch entweder tatsächlich standardisiert ist 
(wie z.B. [Oauth 2](https://oauth.net/2/) und [Openid Connect](https://openid.net/connect/) für den "Türsteher"),
oder zumindestens weit verbreitet (wie z.B. Git, oder Markdown als Datenformat für Inhalte).

Zu einem System wird das Ganze durch Konfigurationseinstellungen der Komponenten 
und durch "Glue-Code" -
das sind kleine Scripte und Progrämmchen, 
die die einzelnen Komponenten miteinander verknüpfen.

### Vorteile
* Viele der Komponenten sind als Standard-Pakete in den meisten Linux-Distributionen enthalten,
  und können daher einfach aktuell gehalten werden
* Die einfachen Schnittstellen zwischen den Komponenten erlauben es,
  bei Bedarf einzelne Teile des Systems auszutauschen zu können.
* Durch den modularen Aufbau kann man das System gut zwischen Sicherheit und
  Bequemlichkeit ausbalancieren.
  Man kann zum Beispiel alle Komponenten auf einem Server und mit demselben 
  Benutzeraccount laufen lassen (bequem für den Administrator, aber nicht 
  sicherer als z.B. ein klassisches CMS), 
  oder jede Komponente auf einen eigenen, abgeschotteten Server auslagern
  (sehr sicher, bedeutet aber mehr Aufwand für den Administrator).
  Oder natürlich irgendetwas dazwischen.

### Nachteile
* So ein System ist nicht trivial aufzusetzen, hier muss man am Anfang einiges an Aufwand treiben
* Die selbstenwickelten Teile des Systems (Konfiguration, Glue-Code, Templates für den Site-Generator,
  CSS und JavaScript-Code für die Webfrontends, ...) müssen nicht nur entwickelt sondern
  auch dauerhaft gepflegt werden
* Wenn man sich ungeschickt anstellt, steht man am Ende statt mit einem gut wartbaren, 
  sicheren System mit einem unwartbaren Script-Verhau da den keiner mehr durchschaut.
