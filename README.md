# Planung, Dokumentation und Code für <https://ftcommunity.de>

Hier soll der Neubau von <https://ftcommunity.de> 
zunächst geplant und später entwickelt, gepflegt und dokumentiert werden.

## Aktueller Stand

* [ ] Planung: Was soll die neue Website können? [Details hier](doc/requirements.md)
* [ ] Planung: Wie wollen wir das umsetzen? [Details hier](doc/architecture.md)
* [ ] Umsetzung
* [ ] Wartung und Pflege
